const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]


/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/

// Q1. Find all the items with price more than $65.
function itemPriceAbove(products, price) {
    const productsObj = products[0]
    // console.log(productsObj)
    return Object.keys(productsObj).reduce((accu1, productName) => {
        // console.log(productName)
        if (Array.isArray(productsObj[productName])) {
            const untensilArr = productsObj[productName]

            accu1[0][productName] = untensilArr.reduce((accu2, utensil) => {
                if (parseInt(Object.values(utensil)[0].price.replace('$', '')) > price) {
                    accu2.push(utensil)
                }
                return accu2
            }, [])
        } else {
            if (parseInt(productsObj[productName].price.replace('$', '')) > price) {
                accu1[0][productName] = productsObj[productName]
            }
        }

        return accu1
    }, [{}])
}

const itemAbove65 = itemPriceAbove(products, 65)
// console.log(itemAbove65)

// Q2. Find all the items where quantity ordered is more than 1.
function itemQuantityAbove(products, quantity) {
    const productsObj = products[0]
    // console.log(productsObj)
    return Object.keys(productsObj).reduce((accu1, productName) => {
        // console.log(productName)
        if (Array.isArray(productsObj[productName])) {
            const untensilArr = productsObj[productName]

            accu1[0][productName] = untensilArr.reduce((accu2, utensil) => {
                if (Object.values(utensil)[0].quantity > quantity) {
                    accu2.push(utensil)
                }
                return accu2
            }, [])
            // console.log(accu1[productName])
        } else {
            if (productsObj[productName].quantity > quantity) {
                accu1[0][productName] = productsObj[productName]
            }
        }

        return accu1
    }, [{}])
}

const itemQuantityAbove1 = itemQuantityAbove(products, 1)
// console.log(itemQuantityAbove1)

// Q.3 Get all items which are mentioned as fragile.
function allFragile(products, type) {
    const productsObj = products[0]
    // console.log(productsObj)
    return Object.keys(productsObj).reduce((accu1, productName) => {
        // console.log(productName)
        if (Array.isArray(productsObj[productName])) {
            const untensilArr = productsObj[productName]

            accu1[0][productName] = untensilArr.reduce((accu2, utensil) => {
                if (Object.values(utensil)[0].type === type) {
                    accu2.push(utensil)
                }
                return accu2
            }, [])
            // console.log(accu1[productName])
        } else {
            if (productsObj[productName].type === type) {
                accu1[0][productName] = productsObj[productName]
            }
        }

        return accu1
    }, [{}])
}

const fragileItems = allFragile(products, 'fragile')
// console.log(fragileItems)


// Q.4 Find the least and the most expensive item for a single quantity.
function leastAndMostExpensive(products) {
    const productsObj = products[0]
    const prices = Object.keys(productsObj).reduce((accu1, productName) => {
        // console.log(productName)
        if (Array.isArray(productsObj[productName])) {
            const untensilArr = productsObj[productName]

            const utensilsPrices = untensilArr.reduce((accu2, utensil) => {
                if (Object.values(utensil)[0].quantity === 1) {
                    accu2.push(parseInt(Object.values(utensil)[0].price.replace("$", '')))
                }
                return accu2
            }, [])
            accu1.push(utensilsPrices)
            // console.log(accu1[productName])
        } else {
            if (productsObj[productName].quantity === 1) {
                accu1.push(parseInt(productsObj[productName].price.replace('$', '')))
            }
        }
        return accu1
    }, [])
        .flat()
        .sort((price1, price2) => {
            return price1 - price2
        })
    const min = prices[0]
    const max = prices[prices.length - 1]

    return Object.keys(productsObj).reduce((accu1, productName) => {
        // console.log(productName)
        if (Array.isArray(productsObj[productName])) {
            const untensilArr = productsObj[productName]

            accu1[0][productName] = untensilArr.reduce((accu2, utensil) => {
                if (parseInt(Object.values(utensil)[0].price.replace('$', '')) === min || parseInt(Object.values(utensil)[0].price.replace('$', '')) === max) {
                    accu2.push(utensil)
                }
                return accu2
            }, [])
            // console.log(accu1[productName])
        } else {
            if (parseInt(productsObj[productName].price.replace('$', '')) === min || parseInt(productsObj[productName].price.replace('$', '')) === max) {
                accu1[0][productName] = productsObj[productName]
            }
        }

        return accu1
    }, [{}])

}

const leastAndMost = leastAndMostExpensive(products)
// console.log(leastAndMost)


// Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)
function groupByState(products) {
    const productsObj = products[0]
    const liquid = ['shampoo', "Hair-oil",]
    const solid = ['comb', 'utensils', 'watch']
    const gas = []
    // console.log(productsObj)
    return Object.keys(productsObj).reduce((accu1, productName) => {
        // console.log(productName)
        if (Array.isArray(productsObj[productName])) {
            const untensilArr = productsObj[productName]
            if (solid.includes(productName)) {
                accu1[0]['solid'][productName] = untensilArr.reduce((accu2, utensil) => {
                    if (Object.values(utensil)[0].quantity === 1) {

                        accu2.push(utensil)
                    }
                    return accu2
                }, [])
            } else if (liquid.includes(productName)) {
                accu1[0]['liquid'][productName] = untensilArr.reduce((accu2, utensil) => {
                    if (Object.values(utensil)[0].quantity === 1) {

                        accu2.push(utensil)
                    }
                    return accu2
                }, [])
            } else {
                accu1[0]['gas'][productName] = untensilArr.reduce((accu2, utensil) => {
                    if (Object.values(utensil)[0].quantity === 1) {

                        accu2.push(utensil)
                    }
                    return accu2
                }, [])
            }

            // console.log(accu1[productName])
        } else {
            if (solid.includes(productName)) {
                accu1[0]['solid'][productName] = productsObj[productName]
            } else if (liquid.includes(productName)) {
                accu1[0]['liquid'][productName] = productsObj[productName]
            } else {
                accu1[0]['gas'][productName] = productsObj[productName]
            }
        }

        return accu1
    }, [
        {
            'solid': {},
            'liquid': {},
            'gas': {}
        }
    ]
    )
}
const groupedByState = groupByState(products)
// console.log(groupedByState)